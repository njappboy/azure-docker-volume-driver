Installation and Configuration of Azure Docker Volume Driver
-----

[Azure Docker Volume Driver](https://github.com/Azure/azurefile-dockervolumedriver)

### Please check out the following documentation based on your target OS

#### Ubuntu 14.04 or lower (upstart)
[Original Documentation](https://github.com/Azure/azurefile-dockervolumedriver/blob/master/contrib/init/upstart/README.md). 
- Process captured here. Run this script on docker-machine image [`./trusty64/azurefile.sh`](./trusty64/azurefile.sh)

#### Ubuntu 15.04 or higher (systemd)
[Original Documentation](https://github.com/Azure/azurefile-dockervolumedriver/blob/master/contrib/init/systemd/README.md) 
- Process captured here. Run this script on docker-machine image [`./wily64/azurefile.sh`](./wily64/azurefile.sh)

#### Usage

```shell
git clone https://bitbucket.org/njappboy/azure-docker-volume-driver.git
cd azure-docker-volume-driver
cd wily64
chmod +x ./azurefile.sh
./azurefile.sh vhdshoom9l6ujbak851g9wer o6AKorLxUSAMOqz+ie0CovcQbOptDA6ZwSoz3pjz18UEeZBTSC91EF9K0/24yEd/Ao+IWWlsbIuH7BW3qOXtim==
```