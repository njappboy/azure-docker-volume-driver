#!/bin/bash

STORAGEACCOUNT_NAME=$1
STORAGEACCOUNT_KEY=$2
if [ -z "$1" ]; then echo "Storage Account Name not set..." && exit 2 ; fi
if [ -z "$2" ]; then echo "Storage Account Key not set..." && exit 2 ; fi

# colorization
GREEN='\033[0;32m'
NC='\033[0m' # No Color

echo -e "${GREEN}# Install Go / Cifs${NC}"
sudo apt-get install golang cifs-utils -y

echo -e "${GREEN}# Temp Structure${NC}"
TMP=/tmp/azurefile
mkdir -p $TMP
cd $TMP
rm -rf $TMP/src

echo -e "${GREEN}# Get files${NC}"
cd $TMP
git clone https://github.com/Azure/azurefile-dockervolumedriver src/azurefile
export GOPATH=`pwd`
cd src/azurefile
go get github.com/tools/godep
$TMP/bin/godep restore

echo -e "${GREEN}# Build${NC}"
go build

echo -e "${GREEN}# Copy binary${NC}"
AZUREVD="/usr/bin/azurefile-dockervolumedriver"
sudo cp $TMP/src/azurefile/azurefile $AZUREVD
sudo chmod +x $AZUREVD

echo -e "${GREEN}# Setup Service${NC}"
cd $TMP/src/azurefile/contrib/init/upstart
sudo cp azurefile-dockervolumedriver.default /etc/default/azurefile-dockervolumedriver
sudo cp azurefile-dockervolumedriver.conf /etc/init/azurefile-dockervolumedriver.conf

echo -e "${GREEN}# Setting Storage Account Info${NC}"
sudo sed -i "s|youraccount|$1|g" /etc/default/azurefile-dockervolumedriver
sudo sed -i "s|yourkey|$2|g" /etc/default/azurefile-dockervolumedriver

echo -e "${GREEN}# Starting Services${NC}"
sudo initctl reload-configuration
sudo initctl start azurefile-dockervolumedriver
sudo initctl status azurefile-dockervolumedriver