#!/bin/bash

STORAGEACCOUNT_NAME=$1
STORAGEACCOUNT_KEY=$2
if [ -z "$1" ]; then echo "Storage Account Name not set..." && exit 2 ; fi
if [ -z "$2" ]; then echo "Storage Account Key not set..." && exit 2 ; fi

# colorization
GREEN='\033[0;32m'
NC='\033[0m' # No Color

echo -e "${GREEN}# Install Go and Cifs${NC}"
sudo apt-get install golang cifs-utils -y

echo -e "${GREEN}# Temp Structure For Build${NC}"
TMP=/tmp/azurefile
mkdir -p $TMP
cd $TMP
rm -rf $TMP/src

echo -e "${GREEN}# Get Driver Source${NC}"
cd $TMP
git clone https://github.com/Azure/azurefile-dockervolumedriver src/azurefile
export GOPATH=`pwd`
cd src/azurefile

echo -e "${GREEN}# Get GoDep Tooling${NC}"
go get github.com/tools/godep

echo -e "${GREEN}# Restore Go Dependencies${NC}"
$TMP/bin/godep restore

echo -e "${GREEN}# Build Driver${NC}"
go build

echo -e "${GREEN}# Copy Driver binary${NC}"
AZUREVD="/usr/bin/azurefile-dockervolumedriver"
sudo cp $TMP/src/azurefile/azurefile $AZUREVD
sudo chmod +x $AZUREVD
echo -e "${GREEN}# Deployed to ${AZUREVD}${NC}"

echo -e "${GREEN}# Setting up service${NC}"


echo -e "${GREEN}# Staging Configuration${NC}"
# Move to the systemd folder in the cloned repository
cd $TMP/src/azurefile/contrib/init/systemd
sudo cp azurefile-dockervolumedriver.default /etc/default/azurefile-dockervolumedriver
sudo cp azurefile-dockervolumedriver.service /etc/systemd/system/azurefile-dockervolumedriver.service

echo -e "${GREEN}# Setting Storage Account Info${NC}"
sudo sed -i "s|youraccount|$1|g" /etc/default/azurefile-dockervolumedriver
sudo sed -i "s|yourkey|$2|g" /etc/default/azurefile-dockervolumedriver

echo -e "${GREEN}# Starting Services${NC}"
sudo systemctl daemon-reload
sudo systemctl enable azurefile-dockervolumedriver
sudo systemctl start azurefile-dockervolumedriver

echo -e "${GREEN}# Provisioning for ${STORAGEACCOUNT_NAME} completed${NC}"
# sudo wget -qO /usr/bin/azurefile-dockervolumedriver https://github.com/Azure/azurefile-dockervolumedriver/releases/download/0.2.1/azurefile-dockervolumedriver